# this launches the package "main" file, passing any arguments along to the "dart run" command

# Write-Host "There are a total of $($args.count) arguments"
# for ( $i = 0; $i -lt $args.count; $i++ ) {
#     Write-Host "Argument  $i is $($args[$i])"
# } 

try {
    dart run .\bin\yawse_cli.dart $args
} catch {
	## If, at any time, for any code inside of the try block, returns a hard-terminating error
	## PowerShell will divert the code to the catch block which writes to the console
	## and exits the PowerShell console with a 1 exit code.
	Write-Host "Error: $($_.Exception.Message)"
	exit 1
}

## When the code inside of the try/catch/finally blocks completes (error or not),
## exit the PowerShell session with an exit code of 0
exit 0
