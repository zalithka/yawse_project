# System Design Notes

> this document is currently in it's early days, having just been migrated from a half-written GitLab issue description update.. it will be cleaned up a little more soon.

as the name implies, this tool's primary purpose is to extract Windows Spotlight images.. but to make it truly flexible and usable, it also requires configuration. as such, it's API is split into a few different commands.

this is done primarily for performance reasons, moving a great deal of the "heavy lifting" out of the regularly-run `export` command (which was previously the only action), and into a new and very-rarely-run `setup` command, with some helpful utilities thrown into a newly designed `doctor` command.

through use of both global runtime flags and command-specific options, these commands will allow you to configure multiple different "export" jobs in an environment, with a pleasant usage experience that makes everything clear and easy.

## CLI Usage Experience

before we go diving into each command's nitty gritty details, let's first look at some example use cases.

### Example Usage with Zero Configuration

without any custom configuration, run the following command: `yawse export`

as per the internal default options, this will result in the following:

* all landscape wallpapers exported to "My Pictures/Spotlight Wallpapers/landscape"
* all portrait wallpapers exported to "My Pictures/Spotlight Wallpapers/portrait"

### Example Usage

to set some basic configuration, run the following command: `yawse config -d Wallpapers -o landscape -f`

* where `-d` sets the `--destination` to a relative path of `Wallpapers`
* where `-o` limits the `--orientation` to export only `landscape`
* where `-f` enables `--flatten`, preventing the creating of the `landscape` subfolder

this will persist the provided values to storage in your user profile, which changes the default behaviour of `yawse export`, to instead result in the following:

* all landscape wallpapers exported to `My Pictures\Wallpapers`, with no subfolders

> **note:** the exact same result may be achieved without persisting configuration, by explicitly setting the values on each run: `yawse export -d Wallpapers -o landscape -f`

## Global Runtime Flags

the following `yawse help` output shows the currently supported global runtime options:

```
Usage: yawse <command> [arguments]

Global options:
-h, --help       Print this usage information.
-Q, --quiet      shut off all informative logging output, only printing errors if any are thrown.
-S, --silent     shut off all logging output, even swallowing errors if any are thrown.
                 takes precedence over --quiet.
-V, --verbose    increase logging verbosity a little bit, just to see a bit more of the inner workings.. does not print silly log messages.
                 takes precedence over --quiet and --silent.
    --debug      increase logging verbosity for debugging purposes. this prints *everything*...
                 takes precedence over --quiet, --silent, and --verbose.
-n, --dry-run    perform the request operation exactly as instructed, skipping any steps that would result in persistent changes.
```

## CLI Commands

### Primary Spotlight Extractor

this module is where all the real magic happens, the actual export of Spotlight Wallpaper images from your current environment..

the following `yawse help extract` output shows the currently supported command runtime options:

```
Usage: yawse extract [arguments]

Runtime alteration options:
these will override any defaults configured for the current environment.
-d, --destination=<PATH>      the folder path to copy the extracted wallpapers to.
                              non-absolute paths are assumed relative to your user My Pictures folder.
-o, --orientation=<OPTION>    limit the extract process to only the specified wallpaper orientation.
                              [all, landscape, portrait]
-f, --[no-]flatten            prevent the creation of "landscape" and "portrait" subfolders.
```

### Environment Config Manager

this module provides the means to manage custom runtime configurations for the `export` command to use.

the following `yawse help config` output shows the currently supported command runtime options:

```
Usage: yawse config [arguments]

Special Config Actions:
    --reset                   clear the current environment configuration and exit..

Runtime Configuration Options:
-d, --destination=<PATH>      a path to copy the extracted wallpapers to.
                              non-absolute paths are calculated relative to your current user "My Pictures" folder.
-o, --orientation=<OPTION>    limit the extract process to only the specified wallpaper orientation.
                              [landscape, portrait]
-f, --[no-]flatten            when set, this prevents the creation of "landscape" and "portrait" subfolders.

Runtime Configuration Resets:
these will take precedence over the new value flags listed above.
    --no-destination          clear the "destination" config setting
    --no-orientation          clear the "orientation" config setting
    --no-flatten              clear the "flatten" config setting
```

## Persisting Custom User Configuration

the tool runtime configuration is persisted inside a JSON documents in userland, at the path `$USERPROFILE/.yawse/config.json`. the structure of this document will be something like the following:

```json
{
  "assets": "<SPOTLIGHT_ASSETS_PATH>",
  "destination": "<EXPORT_OUTPUT_PATH>",
  "orientation": "['landscape'|'portrait']",
  "flatten": "[true|false]"
}
```

where:

* `assets` is a specific "known path", consistent across all Windows computers. you should **"never"** need to set this again..
* `destination` is calculated to _your user's_ "My Pictures" folder, as read from the Windows Registry.
* `orientation` is a string option. if set, this must be either `landscape` or `portrait`.
* `flatten` is a flag that is not set by default.
   * when `true`, all exported files will go directly into the specified `destination` folder.
   * when `false`, exported files will be placed into `landscape` and `portrait` subfolders of the specified `destination` folder.

## Roadmap / Wishlist

> **disclaimer**: this list is subject to change without notice, based on my own personal whims and desires, augmented by requests from other people. if you have an idea/request/complaint/problem, please open a new issue in GitLab. more issue templates will be added soon to assist with this process.

in no particular order, the following general changes are currently "planned" for this project:

* [ ] cater for persisting multiple userland configurations, named to allow for selection through a runtime argument.
* [ ] include an "interactive mode", which will allow all available configuration to be set by a series of user input prompts.
* [ ] keep a record of all exported file names, to be referenced at runtime to preventing re-exporting previously exported wallpapers.
* [ ] create a GUI wrapper for the `config` subcommand.
