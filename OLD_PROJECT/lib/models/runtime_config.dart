import 'dart:convert';

import 'package:path/path.dart';

/// this class represents the "runtime environment configuration" for this CLI tool.
class RuntimeConfig {
  // here we define the core properties of the runtime config, assigning defaults where possible
  String name;
  String? assetsPath;
  String? destination;
  String? orientation;
  bool? flatten;

  RuntimeConfig(
    this.name, {
    this.assetsPath,
    this.destination,
    this.orientation,
    this.flatten,
  });

  /// this creates a RuntimeConfig instance from the JSON string as read from the YAWSE userland config file
  static RuntimeConfig fromJson(String yawseConfigString) {
    var yawseConfig = JsonDecoder().convert(yawseConfigString);
    return RuntimeConfig(
      yawseConfig?['name'] ?? 'json',
      assetsPath: yawseConfig?['assetsPath'],
      destination: yawseConfig?['destination'],
      orientation: yawseConfig?['orientation'],
      flatten: yawseConfig?['flatten'],
    );
  }

  /// this will create a new RuntimeConfig object, picking the incoming option first, falling back to the target if null.
  static RuntimeConfig merge(dynamic fallback, dynamic incoming) {
    return RuntimeConfig(
      incoming?.name ?? fallback.name,
      assetsPath: incoming?.assetsPath ?? fallback.assetsPath,
      destination: incoming?.assetsPath ?? fallback.destination,
      orientation: incoming?.orientation ?? fallback.orientation,
      flatten: incoming?.flatten ?? fallback.flatten,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'assetsPath': normalize(assetsPath ?? ''),
      'destination': normalize(destination ?? ''),
      'orientation': orientation,
      'flatten': flatten,
    };
  }

  @override
  String toString() {
    JsonEncoder encoder = JsonEncoder.withIndent('  ');
    return encoder.convert(this);
  }
}
