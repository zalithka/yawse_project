import 'dart:convert';

/// this class represents the "runtime environment configuration" for this CLI tool.
class WallpaperMeta {
  // here we define the core properties of the runtime config, assigning defaults where possible
  String name;
  String assetsPath;
  String destination;
  String orientation;

  WallpaperMeta(
    this.name,
    this.assetsPath,
    this.destination,
    this.orientation,
  );

  @override
  String toString() {
    JsonEncoder encoder = JsonEncoder.withIndent('  ');
    return encoder.convert(this);
  }
}
