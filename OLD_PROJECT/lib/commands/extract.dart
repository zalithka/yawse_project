import 'dart:io';

import 'package:args/command_runner.dart';
import 'package:dcli/dcli.dart';
import 'package:image_size_getter/file_input.dart';
import 'package:image_size_getter/image_size_getter.dart';
import 'package:loggy/loggy.dart';
import 'package:mime/mime.dart';
import 'package:quiver/iterables.dart';
import 'package:yet_another_windows_spotlight_extractor/helpers/config.dart';
import 'package:yet_another_windows_spotlight_extractor/helpers/logging.dart';
import 'package:yet_another_windows_spotlight_extractor/models/runtime_config.dart';
import 'package:yet_another_windows_spotlight_extractor/models/wallpaper_meta.dart';

class ExtractCommand extends Command {
  @override
  final name = "extract";

  @override
  final description = "Trigger the primary Spotlight Extractor process.";

  ExtractCommand() {
    argParser
      ..addSeparator('Runtime Configuration Options:')
      ..addOption(
        'destination',
        abbr: 'd',
        valueHelp: 'PATH',
        help: 'the folder path to copy the extracted wallpapers to.\n[*] non-absolute paths are assumed relative to your user My Pictures folder.',
      )
      ..addOption(
        'orientation',
        abbr: 'o',
        valueHelp: 'ORIENTATION',
        allowed: ['all', 'landscape', 'portrait'],
        help: 'limit the extract process to only the specified wallpaper orientation.\nthe "all" option in this case overrides any inherited config.',
      )
      ..addFlag(
        'flatten',
        abbr: 'f',
        help: 'prevent the creation of "landscape" and "portrait" subfolders.',
      )
      ..addFlag(
        'overwrite',
        abbr: 'w',
        negatable: false,
        help: 'when set, causes the file extractor to overwrite destination filenames if they already exist.',
      )
      ..addOption(
        'pause',
        abbr: 'p',
        valueHelp: 'SECONDS',
        help: 'when set, the file extractor will remain open after it has run, showing a summary of what it did.',
      );
  }

  @override
  Future<void> run() async {
    initLoggyFromRuntimeFlags(globalResults);

    String? argsDestination;
    String? argsOrientation;
    bool? argsOverwrite;
    bool? argsFlatten;
    int? pauseOnComplete;

    if (argResults!.wasParsed('destination')) {
      logDebug('parsed destination option: ${argResults?['destination']}');
      argsDestination = argResults?['destination'];
    }
    if (argResults!.wasParsed('orientation')) {
      logDebug('parsed orientation option: ${argResults?['orientation']}');
      argsOrientation = argResults?['orientation'];
      if (argsOrientation == 'all') {
        argsOrientation = null;
      }
    }
    if (argResults!.wasParsed('overwrite')) {
      logDebug('parsed overwrite flag: ${argResults?['overwrite']}');
      argsOverwrite = argResults?['overwrite'];
    }
    if (argResults!.wasParsed('flatten')) {
      logDebug('parsed flatten flag: ${argResults?['flatten']}');
      argsFlatten = argResults?['flatten'];
    }
    if (argResults!.wasParsed('pause')) {
      logDebug('parsed pause option: ${argResults?['pause']}');
      pauseOnComplete = int.tryParse(argResults?['pause']);
      if (pauseOnComplete != null) {
        if (pauseOnComplete.isNegative) {
          logWarning('note: user prompted pause support coming soon..');
          pauseOnComplete = 0;
        }
        if (pauseOnComplete > 10) {
          logWarning('note: pause duration has been limited to 10 seconds.');
          pauseOnComplete = 10;
        }
      }
    }

    bool haveArgsInput = [argsDestination, argsOrientation, argsFlatten].any((element) => element != null);

    // inject current runtime argument options into current runtime config
    RuntimeConfig effectiveConfig = expandArgsToEffectiveConfig(RuntimeConfig(
      'runtime',
      destination: argsDestination,
      orientation: argsOrientation,
      flatten: argsFlatten,
    ));
    if (haveArgsInput) {
      logInfo('custom runtime config: $effectiveConfig');
    } else {
      logDebug('effective runtime config: $effectiveConfig');
    }

    // ! get list of files from assets folder
    List<FileSystemEntity> spotlightAssetsEntities = Directory(effectiveConfig.assetsPath!).listSync();
    // ! filter to only wallpaper files (only images and not square)
    List<WallpaperMeta> landscapeWallpapers = [];
    List<WallpaperMeta> portraitWallpapers = [];
    for (final FileSystemEntity file in spotlightAssetsEntities) {
      if (file is File) {
        logDebug('processing file: ${file.path}');
        String? mime = lookupMimeType(file.path, headerBytes: [0xFF, 0xD8]);
        logDebug('detected MIME type as: $mime');
        Size size = ImageSizeGetter.getSize(FileInput(file));
        logDebug('detected dimensions as: ${size.width} x ${size.height}');

        String fileName = '${file.path.split(r"\").last}.${mime?.split(r"/").last}'.replaceAll('jpeg', 'jpg');

        if (size.width > size.height) {
          // if it's wider..
          landscapeWallpapers.add(WallpaperMeta(fileName, file.path, effectiveConfig.destination!, 'landscape'));
        }

        if (size.width < size.height) {
          // portrait
          portraitWallpapers.add(WallpaperMeta(fileName, file.path, effectiveConfig.destination!, 'portrait'));
        }
      }
    }
    logDebug('now we have ${landscapeWallpapers.length} landscape and ${portraitWallpapers.length} portrait wallpapers to work with');

    List<WallpaperMeta> desiredWallpapers;

    // ! copy to configured destination folder(s) with file extension (extracted from file MIME data)
    switch (effectiveConfig.orientation) {
      case 'landscape':
        desiredWallpapers = landscapeWallpapers;
        break;
      case 'portrait':
        desiredWallpapers = portraitWallpapers;
        break;
      default:
        desiredWallpapers = concat([landscapeWallpapers, portraitWallpapers]).toList();
        break;
    }

    // ! a great deal of this must be abstracted away into a helper class..
    desiredWallpapers.forEach((WallpaperMeta wall) {
      String destination = normalize((effectiveConfig.flatten == true) ? wall.destination : '${wall.destination}/${wall.orientation}');

      if (globalResults!['dry-run']) {
        logWarning('[dry-run] skipping extraction of:\n"$destination\\${wall.name}"..');
      } else {
        Directory destinationEntity = Directory(destination);
        if (!destinationEntity.existsSync()) {
          logDebug('destination path "${destinationEntity.path}" does not exist yet. creating it now..');
          destinationEntity.createSync(recursive: true);
        }

        try {
          copy(
            wall.assetsPath,
            '${destinationEntity.path}/${wall.name}',
            overwrite: argsOverwrite ?? false,
          );
          logInfo('extracted wallpaper "${wall.name}"..');
        } catch (error) {
          if (error.toString().contains('already exists.')) {
            return logInfo('skipping "${wall.name}" as it already exists..');
          }
          logError('unexpected error trying to copy file: (${error.runtimeType}) $error');
        }
      }
    });

    if (argResults!.wasParsed('pause') && pauseOnComplete != null) {
      // await specified duration
      logDebug('pausing for $pauseOnComplete seconds for user feedback..');
      await Future.delayed(Duration(seconds: pauseOnComplete));
    }
  }
}
