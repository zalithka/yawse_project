# Yet _Another_ Windows Spotlight Extractor..

> ..because reasons (:

## Introductions

this project is a number of different things to me _personally_, but I have decided to keep it open source, also because of what this project is to me.

the _really_ short version: I had previously built something like this as a NodeJS based CLI tool. however, the overhead of having to bundle NodeJS itself _inside_ the `exe` for standalone distribution was, well, painful.. not to mention the size of the native bindings in some packages, a few of which this was starting to require.

anyways, this is a Dart rewrite of _that_ project, which has already at the time of writing this, become more than it's predecessor was.

## Core Runtime Overview

> note: future plans can be seen in the [`design document`](./DESIGN.md).

the primary purpose of this tool, is to extract appropriate wallpapers images into a custom folder. which happens in the following simple steps:

1. **load environment configuration, reading from user configuration file and runtime arguments.**
2. **identify the files in the Spotlight assets folder that contain image data.**
   * _**fair warning:** this filters out the icons and branding assets that exist in the same folder, based on the premise that they are always square aspect ratios... I can't stop MS from one day doing something different here._
3. **copy newly discovered wallpapers to the configured output path, optionally filtered by image orientation.**

> **first things first**, there are some key points that must be clarified here first: existing target files will **not be overwritten**. but this is only a blind file name match, this tool *does not* and *will not* scan the target output folder to detect duplicates.. this is a CPU-costly procedure to execute, and a PITA to have to build in _here_.

## Usage Experience Points

so, beyond "simply" extracting image files from one known path to another, this project sets out to solve a few more specific use-case problems as well. but instead of describing each of those, I will instead list the core design elements that not only drove it to work as it is now, but also what it will become.

1. **It must have super simple usage...**

by default, running the main `yawse extract` command will "Just Work"(tm).. this is based on the following assumptions made in-app:

* that the current environment is running Windows 10 with the Spotlight lockscreen image service enabled..
  * so this looks at a "known path" deep within `%LOCALAPPDATA%`.
* that you _might prefer_ your pictures to be in your "My Pictures" folder..
  * so this reads your profile's "My Pictures" path from the `HKCU` hive in Windows Registry, extracting to a `Spotlight Wallpapers` subfolder within that.
* that you _might like_ to have the landscape and portrait wallpapers separated..
  * so this copies each orientation image to the appropriately named `landscape` or `portrait` subfolder therein.

without any customisation, this gives you a local folder that you can use as a source for randomised desktop wallpapers. along with another folder for your phone, easy to synchronise to any cloud service to get delivered to your pocket. :)

2. **It must be flexible...**

the `yawse extract` command also accepts a number of runtime arguments, each of which control a critical aspect of the above default workflow.

* `--destination` (or `-d`) sets a custom path to extract wallpapers to.
  * if this is a relative path, it will be appended to your user profile "My Pictures" folder path.
* `--orientation` (or `-o`) can be set to `landscape` or `portrait`, limiting the wallpapers that are actually extracted.
* `--flatten` (or `-f`) will prevent the creation of the `landscape` and `portrait` subfolders during the extraction process.
  * if `--orientation` has been set, then this is automatically enabled.
  * if `--orientation` is not set, this will place landscape and portrait wallpapers at the root of the configured destination path.

as one example, running the command `yawse extract -d "Desktop Wallpapers" -o landscape` will extract only landscape orientation wallpapers directly into your profile's "My Pictures\Desktop Wallpapers" folder.

in another example, running the command `yawse extract -d "Awesome Pictures" -f` will extract both landscape and portrait wallpapers side-by-side into your profile's "My Pictures\Awesome Pictures" folder.

3. **It must be highly configurable...**

the sibling `yawse config` command takes these same runtime arguments, and instead persists them to a configuration file within your user profile, which is in turn read by the `yawse extract` command when _it_ runs.

these persisted settings do nothing but change the default fallback value for `yawse extract`, in case it's not specified by it's appropriate runtime argument.

also, since settings can be both _set_ and _unset_, this does include a few variations as well, specifically for clearing custom values:

* `--no-destination` will clear the current custom output destination
* `--no-orientation` will clear the current orientation output filter
* `--no-flatten` will clear the current explicit `true` or `false` value
* `--reset` will clear all custom configuration, specifically by removing the user configuration file that it created.

> disclaimer: all `--no-[prop]` arguments will override their accompanying `--[prop]` argument. calling them together is redundant. `--reset` takes precedence over all other arguments, literally killing the user config file, printing out the new environment "runtime config" state, and exiting.

as one example use case, let's set the values from the first custom runtime above as the new defaults, by calling `yawse config -d "Desktop Wallpapers" -o landscape`. with this in place, running the `yawse extract` command with no additional arguments will achieve the same output as before.

with this same config in place, running the `yawse extract -d "D:\Backups\Wallpapers"` will copy new landscape wallpapers to "D:\Backups\Wallpapers" instead.

this **does not** attempt to handle recursive relative destination settings.. in both `extract` and `config`, **any** relative path will be taken as relative to your profile's "My Pictures" folder path, wherever you have that set to.
