import 'package:args/command_runner.dart';
import 'package:dcli/dcli.dart';
import 'package:loggy/loggy.dart';
import 'package:yet_another_windows_spotlight_extractor/commands/config.dart';
import 'package:yet_another_windows_spotlight_extractor/commands/extract.dart';

Future<void> main(List<String> arguments) async {
  Loggy.initLoggy(
    logPrinter: PrettyPrinter(showColors: true),
    logOptions: LogOptions(LogLevel.info, stackTraceLevel: LogLevel.error),
  );

  // grab a runner instance..
  CommandRunner yawseRunner = CommandRunner(
    'yawse',
    'Yet Another Windows Spotlight Extractor.',
  );

  // ..attach global CLI options..
  yawseRunner.argParser
    ..addFlag(
      'silent',
      abbr: 'S',
      negatable: false,
      defaultsTo: false,
      help: 'shut off all logging output, even swallowing errors if any are thrown.\nthis sets the logging level to "off".',
    )
    ..addFlag(
      'verbose',
      abbr: 'V',
      negatable: false,
      defaultsTo: false,
      help:
          'increase logging verbosity, mainly useful for debugging runtime issues.\n[!] this takes precedence over --silent.\nthis sets the logging level to "all".',
    )
    ..addFlag(
      'dry-run',
      abbr: 'n',
      negatable: false,
      defaultsTo: false,
      help: 'perform the request operation exactly as instructed, skipping any steps that would result in persistent changes.',
    );

  // ..attach our CLI commands..
  yawseRunner
    ..addCommand(ConfigCommand())
    ..addCommand(ExtractCommand());

  logInfo('launching ${yawseRunner.description}');

  List<String> effectiveArguments = arguments;

  // ..and see what the terminal god gave us
  if (effectiveArguments.isEmpty) {
    // we got no CLI arguments, prompt the user for an action..
    logDebug('no runtime command provided, running in interactive mode..');
    var menuChoice = menu(
      prompt: '[?] which command would you like to run?',
      options: ['extract', 'config', 'help'],
      defaultOption: 'help',
    );
    switch (menuChoice) {
      case 'extract':
        effectiveArguments = ['extract'];
        break;
      case 'config':
        effectiveArguments = ['config'];
        break;
      case 'help':
        effectiveArguments = ['help'];
        break;
    }
  }

  await yawseRunner.run(effectiveArguments).catchError((error) {
    if (error.runtimeType == UsageException) {
      logInfo("${error.runtimeType}: $error");
    } else {
      logError("[fatal] ${error.runtimeType}: $error");
    }
  });
}
