# YAWSE - User Environment Convenience Wrapper

this Dart package exports structured methods for interacting with the current system host, providing a means to retrieve relevant information from host system resources.

for the sake of transparency, this includes the following sources:

* **Windows Registry** - from which the current user's "My Pictures" path is read, because this can be customised on the host.
* **User AppData Folder** - because this is where the source images are saved by Windows.
* **User Profile Folder** - because this is where the YAWSE configuration file will live.

on that last note, the current runtime settings include:

* **Output Folder** (folder path) - is the target folder to use when extracting wallpapers.
* **Filter Orientation** (select list) - select either `landscape` or `portrait` to restrict the extractor output to.
  * when not set, this will extract both.
  * _NOTE: using this option implies `--flatten`, as you probably don't want a `landscape` or `portrait` subfolder if you're only extracting one size..._
* **Flatten Output** (toggle) - whether or not to flatten the extractor output.
  * when enabled, this prevents the creation of the `landscape` and `portrait` subfolders.
  * _NOTE: when used without an orientation filter, this will cause both `landscape` and `portrait` wallpapers to be placed in the same folder, which you probably don't want..._
* **Runtime Logging** (folder path) - is the folder to use to write runtime log files.
  * _NOTE: these are written using rolling appenders, with individual file sizes limited to `5mb` and automatically deleted at 1 week old, because **nobody** likes big log folders... more config options may come in the future._
