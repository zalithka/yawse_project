# YAWSE - Extractor Runtime Business Logic

this Dart package exports structured methods with the logic to _actually extract_ the wallpapers, governed by configuration options in the current runtime environment.

where this "runtime configuration" is built up from the following layers:

* **Internal Defaults** - these are selected in a way that provides the best value for money, but may not work for everybody.
* **User Configuration** - which exist within text-based files in your user environment, read and updated by the various components at runtime.
* **Runtime Arguments** - at least in the case of the CLI tool, any configuration option may be explicitly overridden for _only that runtime_.
