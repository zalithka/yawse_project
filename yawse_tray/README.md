# YAWSE - System Tray Tool

this Flutter application runs in the current user session, with a System Tray icon pinning it to the Shell, and a context menu for feature access.

the context menu options include:

* **Runtime Configuration** - which will show the standard Configuration Tool window, plus a few extra options specifically for the System Tray application.
* **View Log Folder** - which will open the configured log file folder in Explorer. if none is configured, a message will be shown to say so.
* **Run Extractor** - which will launch the CLI Tool.
* **Exit** - which will shut down the System Tray Tool.

as for the System Tray Tool extra options, because this application is designed to run in the "background", it makes use of this persistent state by monitoring the Windows Spotlight `Assets` folder. this provides a mechanism to trigger the extraction of wallpaper images, effectively as soon as they're downloaded..

because this process will happen silently in the background, an option will be provided to show a desktop notification when this happens. when enabled, this will show a basic summary of how many images were found and extracted after it completes.
